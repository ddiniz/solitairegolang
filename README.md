# SolitaireGolang

Solitaire implemented in golang, SDL and terminal visualizations (change in main.go, SDL by default)

Game rules are the same as Zachtronic's Sawayama solitaire

# Prerequisites

Golang 1.18

SDL2 and SDL2-image

# Running for Terminal

```
go run -tags terminal source/cmd/app/main.go
```

# Running for GUI

```
go run -tags sdl source/cmd/app/main.go
```

# Build for Terminal

```
go build -tags terminal source/cmd/app/main.go
```

# Build for GUI

```
go build -tags sdl source/cmd/app/main.go
```

# License

Do what you want with the code and assets, just give us credit.

Code - Davi Diniz and Luis Guilherme Martins

Assets - Davi Diniz and Luis Guilherme Martins
