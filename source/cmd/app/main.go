package main

import (
	"math/rand"
	"solitairegolang/source/runner"
	"time"
)

// TODO: game solver
// TODO: Check if there are no more valid moves possible
// TODO: refactor whole code, it's getting dirty
// TODO: Game sprites should be all in one big spritesheet, cards should be created programatically
// TODO: test suite after it's done
// TODO: animations?
// TODO: choose difficulty (number of cards that are drawn from stock)
// TODO: State machine for cards

func main() {
	rand.Seed(time.Now().UnixNano())
	runner.RunGame()
}
