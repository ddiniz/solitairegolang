package card

import "log"

func SuitToColor(suit Suit) Color {
	switch suit {
	case Spade, Club:
		return Black
	case Heart, Diamond:
		return Red
	default:
		log.Printf("Invalid suit to color: '%d', defaulting to Black\n", suit)
		return Black
	}
}

func EmptyCard() string {
	return "___"
}

func suitToText(suit Suit) string {
	switch suit {
	case Club:
		return "C"
	case Heart:
		return "H"
	case Spade:
		return "S"
	case Diamond:
		return "D"
	}
	log.Printf("Invalid suit: '%d'\n", suit)
	return "?"
}

func rankToText(rank Rank) string {
	switch rank {
	case King:
		return "_K"
	case Queen:
		return "_Q"
	case Jack:
		return "_J"
	case Card10:
		return "10"
	case Card9:
		return "_9"
	case Card8:
		return "_8"
	case Card7:
		return "_7"
	case Card6:
		return "_6"
	case Card5:
		return "_5"
	case Card4:
		return "_4"
	case Card3:
		return "_3"
	case Card2:
		return "_2"
	case CardA:
		return "_A"
	}
	log.Printf("Invalid rank: '%d' \n", rank)
	return "?"
}

func runeToSuit(suitRune rune) Suit {
	switch suitRune {
	case 'C':
		return Club
	case 'H':
		return Heart
	case 'S':
		return Spade
	case 'D':
		return Diamond
	}
	log.Printf("Invalid suit: '%c' ,  defaulting to Club\n", suitRune)
	return Club
}

func rankToSuit(rankStr string) Rank {
	switch rankStr {
	case "_K":
		return King
	case "_Q":
		return Queen
	case "_J":
		return Jack
	case "10":
		return Card10
	case "_9":
		return Card9
	case "_8":
		return Card8
	case "_7":
		return Card7
	case "_6":
		return Card6
	case "_5":
		return Card5
	case "_4":
		return Card4
	case "_3":
		return Card3
	case "_2":
		return Card2
	case "_A":
		return CardA
	}
	log.Printf("Invalid rank: '%s' ,  defaulting to cardA\n", rankStr)
	return CardA
}
