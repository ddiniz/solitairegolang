package card

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSuitToColor(t *testing.T) {
	colors := []Color{
		Red,
		Black,
		Black,
		Red,
	}
	for i, suit := range Suits {
		color := SuitToColor(suit)
		assert.Equal(t, color, colors[i])
	}
}

func TestSuitToText(t *testing.T) {
	texts := []string{
		"H",
		"C",
		"S",
		"D",
	}
	for i, suit := range Suits {
		text := suitToText(suit)
		assert.Equal(t, text, texts[i])
	}

	text := suitToText(99)
	assert.Equal(t, text, "?")
}

func TestRankToText(t *testing.T) {
	texts := []string{
		"_K",
		"_Q",
		"_J",
		"10",
		"_9",
		"_8",
		"_7",
		"_6",
		"_5",
		"_4",
		"_3",
		"_2",
		"_A",
	}
	for i := range Ranks {
		text := rankToText(Ranks[i])
		assert.Equal(t, text, texts[i])
	}

	text := suitToText(99)
	assert.Equal(t, text, "?")
}

func TestTextToCardAndCardToText(t *testing.T) {
	texts := make([]string, 0)
	for _, rank := range Ranks {
		for _, suit := range Suits {
			texts = append(texts, suitToText(suit)+rankToText(rank))
		}
	}

	for _, tex := range texts {
		card := NewCard(tex)
		text := card.ToText()
		assert.Equal(t, tex, text)
	}
}
