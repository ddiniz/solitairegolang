package card

import (
	"log"
	h "solitairegolang/source/hitbox"
)

type Suit uint8
type Color uint8
type Rank uint8

const (
	CARD_WIDTH  int32 = 112
	CARD_HEIGHT int32 = 160
)

const (
	Heart Suit = iota
	Club
	Spade
	Diamond
)

const (
	Red Color = iota
	Black
)

const (
	CardA Rank = iota
	Card2
	Card3
	Card4
	Card5
	Card6
	Card7
	Card8
	Card9
	Card10
	Jack
	Queen
	King
)

var Suits = []Suit{
	Heart,
	Club,
	Spade,
	Diamond,
}

var Ranks = []Rank{
	King,
	Queen,
	Jack,
	Card10,
	Card9,
	Card8,
	Card7,
	Card6,
	Card5,
	Card4,
	Card3,
	Card2,
	CardA,
}

type Card struct {
	Suit        Suit          `json:"suit"`
	Rank        Rank          `json:"rank"`
	Color       Color         `json:"color"`
	Hitbox      *h.Hitbox     `json:"hitbox"`
	TexturePos  *h.Hitbox     `json:"texturePos"`
	TextureName string        `json:"textureName"`
	Col         int           `json:"col"`
	Height      int           `json:"height"`
	State       CardStateEnum `json:"state"`
	LastState   CardStateEnum `json:"lastState"`
}

func (card *Card) ToText() string {
	return suitToText(card.Suit) + rankToText(card.Rank)
}

func NewEmptyCard(rank Rank, suit Suit) *Card {
	return &Card{
		Suit:        suit,
		Rank:        rank,
		Color:       SuitToColor(suit),
		Hitbox:      &h.Hitbox{X: 0, Y: 0, W: 0, H: 0},
		TextureName: suitToText(suit) + rankToText(rank),
		State:       HIDDEN_STOCK,
		LastState:   HIDDEN_STOCK,
	}
}

func NewCard(str string) *Card {
	if len(str) == 0 {
		log.Fatalf("Trying to create a card with empty string: %s\n", str)
	}

	if str == EmptyCard() {
		return nil
	}

	suit := runeToSuit(rune(str[0]))
	rank := rankToSuit(str[1:])
	color := SuitToColor(suit)
	return &Card{suit, rank, color, nil, nil, "", -1, -1, HIDDEN_STOCK, HIDDEN_STOCK}
}
