package card

import "log"

type CardStateEnum uint8

const (
	HIDDEN_STOCK CardStateEnum = iota
	STOCK
	BOARD
	FOUNDATION
	OPEN_STOCK
	HAND
)

func printBadState(card *Card, next CardStateEnum) {
	log.Println("Bad card state, current:", card.State, " next:", next)
}

func (card *Card) chageStateUpdateLastState(next CardStateEnum) {
	card.LastState = card.State
	card.State = next
}

func (card *Card) ChangeState(next CardStateEnum) {
	switch card.State {
	case HIDDEN_STOCK:
		switch next {
		case HIDDEN_STOCK:
			card.chageStateUpdateLastState(HIDDEN_STOCK)
		case STOCK:
			card.chageStateUpdateLastState(STOCK)
		default:
			printBadState(card, next)
		}
	case STOCK:
		switch next {
		case STOCK:
			card.chageStateUpdateLastState(STOCK)
		case FOUNDATION:
			card.chageStateUpdateLastState(FOUNDATION)
		case HAND:
			card.chageStateUpdateLastState(HAND)
		default:
			printBadState(card, next)
		}
	case BOARD:
		switch next {
		case BOARD:
			card.chageStateUpdateLastState(BOARD)
		case FOUNDATION:
			card.chageStateUpdateLastState(FOUNDATION)
		case HAND:
			card.chageStateUpdateLastState(HAND)
		default:
			printBadState(card, next)
		}
	case OPEN_STOCK:
		switch next {
		case OPEN_STOCK:
			card.chageStateUpdateLastState(OPEN_STOCK)
		case FOUNDATION:
			card.chageStateUpdateLastState(FOUNDATION)
		case HAND:
			card.chageStateUpdateLastState(HAND)
		default:
			printBadState(card, next)
		}
	case FOUNDATION:
		switch next {
		case FOUNDATION:
			card.chageStateUpdateLastState(FOUNDATION)
		default:
			printBadState(card, next)
		}
	case HAND:
		switch next {
		case HAND:
			card.chageStateUpdateLastState(HAND)
		case BOARD:
			card.chageStateUpdateLastState(BOARD)
		case OPEN_STOCK:
			card.chageStateUpdateLastState(OPEN_STOCK)
		case STOCK:
			card.chageStateUpdateLastState(STOCK)
		default:
			printBadState(card, next)
		}
	}
}
