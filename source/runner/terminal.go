//go:build terminal
// +build terminal

package runner

import (
	"solitairegolang/source/terminal"
)

func init() {
	RunGame = terminal.RunGame
}
