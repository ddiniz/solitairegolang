//go:build sdl
// +build sdl

package runner

import (
	"solitairegolang/source/sdl"
)

func init() {
	RunGame = sdl.RunGame
}
