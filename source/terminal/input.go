//go:build terminal
// +build terminal

package terminal

import (
	"fmt"
	"strings"
)

func captureInput() (string, []string) {
	input := ""
	fmt.Scan(&input)
	args := strings.Split(input, "_")
	return strings.ToLower(input), args
}
