//go:build terminal
// +build terminal

package terminal

import (
	"fmt"
	"log"
	"os"
	g "solitairegolang/source/game"
	"strconv"
)

func RunGame() {
restart:
	game := g.NewGame(7, 21, 3)
	game.InitializeGameKlondike()

	for {
		game.TryToMoveAllTopCardsToFoundations()
		renderGame(game)

		gameEnd := game.IsGameEnded()
		if gameEnd {
			fmt.Println("VICTORY! Start new game(Y/N)?")
			input, _ := captureInput()
			if input == "y" || input == "yes" {
				goto restart
			} else {
				os.Exit(0)
			}
		}

		fmt.Print("Type a command (ex:'help'): ")
		input, args := captureInput()

		println(input)
		switch args[0] {
		case "q", "quit":
			os.Exit(0)

		case "m", "move":
			if len(args) == 4 {
				sourceCol, _ := strconv.Atoi(args[1])
				sourceHeight, _ := strconv.Atoi(args[2])
				destination, _ := strconv.Atoi(args[3])
				game.MoveCardInBoard(sourceCol, sourceHeight, destination)
				break
			}

			if len(args) == 3 {
				sourceCol, _ := strconv.Atoi(args[1])
				destination, _ := strconv.Atoi(args[2])
				game.MoveCardInBoard(sourceCol, -1, destination)
				break
			}

			log.Printf("Invalid move args.\n")
			log.Printf("          USAGE: move_<source>_<destination>\n")
			log.Printf("                 move_<sourceCol>_<sourceHeight>_<destination>\n")
		case "ms", "movestock":
			if len(args) != 2 {
				log.Printf("Invalid movestock args.\n")
				log.Printf("          USAGE: movestock_<destination>\n")
			}
			destinationCol, _ := strconv.Atoi(args[1])
			game.MoveCardFromStock(destinationCol)
		case "s", "stock":
			game.PullCardsHiddenStock()

		case "r", "restart":
			goto restart

		case "h", "help":
			log.Printf("HELP:\n")
			log.Printf("move /  m:\n")
			log.Printf("          USAGE: move_<source>_<destination>\n")
			log.Printf("                 move_<sourceCol>_<sourceHeight>_<destination>\n")
			log.Printf("restart /  r: Restarts the game\n")
			log.Printf("quit /  q: Quits the game\n")
			log.Printf("stock / s: Pulls 3 cards from the stock or whatever remains.\n")
		}
	}
}
