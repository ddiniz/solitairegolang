//go:build terminal
// +build terminal

package terminal

import (
	"fmt"
	c "solitairegolang/source/card"
	g "solitairegolang/source/game"
	u "solitairegolang/source/utils"

	chalk "github.com/ttacon/chalk"
)

var _red = chalk.Red.NewStyle()
var _white = chalk.White.NewStyle()

func renderGame(game *g.Game) {
	renderGameHeader(game)
	renderBoardHeader(game)
	for j := 0; j < game.MaxColHeight; j++ {
		fmt.Printf("%2d  ", j)
		for i := 0; i < len(game.Board); i++ {
			if j < len(game.Board[i]) && game.Board[i][j] != nil {
				renderColoredCard(game.Board[i][j])
				fmt.Printf(" ")
				continue
			}

			fmt.Printf(c.EmptyCard() + " ")
		}
		fmt.Printf("%2d\n", j)
	}
}

func renderGameHeader(game *g.Game) {
	fmt.Printf("BOARD:")
	for i := 0; i < game.Cols+1; i++ {
		fmt.Printf("    ")
	}
	fmt.Printf("\t")
	renderStock(game)
	fmt.Printf("\n")
}

func renderBoardHeader(game *g.Game) {
	fmt.Printf("   ")
	for i := 0; i < game.Cols; i++ {
		fmt.Printf("  %d ", i)
	}
	fmt.Printf("\t\t")
	renderFoundations(game)
	fmt.Printf("\n")
}

func renderStock(game *g.Game) {
	if u.IsEmpty(game.HiddenStock) {
		fmt.Printf("STOCK (Empty):")
	} else {
		fmt.Printf("STOCK:")
	}
	for i := 0; i < len(game.Stock); i++ {
		renderColoredCard(game.Stock[i])
		fmt.Printf(" ")
	}
}

func renderFoundations(game *g.Game) {
	fmt.Printf("Foundations: ")
	for i := 0; i < len(game.Foundations); i++ {
		if len(game.Foundations[i]) > 0 {
			topCard := game.Foundations[i][len(game.Foundations[i])-1]
			renderColoredCard(topCard)
			fmt.Printf(" ")
			continue
		}

		fmt.Printf("%s ", c.EmptyCard())
	}
}

func renderColoredCard(card *c.Card) {
	switch card.Color {
	case c.Red:
		fmt.Printf("%s%s%s", _red, card.ToText(), chalk.Reset)
	case c.Black:
		fmt.Printf("%s%s%s", _white, card.ToText(), chalk.Reset)
	}
}
