package hitbox

type Hitbox struct {
	X int32
	Y int32
	W int32
	H int32
}

func (hitbox *Hitbox) InHitbox(x int32, y int32) bool {
	return hitbox != nil && checkBound(x, hitbox.X, hitbox.W) && checkBound(y, hitbox.Y, hitbox.H)
}
func checkBound(val, x, w int32) bool {
	return x <= val && val <= x+w
}
