package game

type DifficultySettings struct {
	HiddenOpenStock         bool
	HiddenStockPullQuantity int
	HiddenStockLoopQuantity int
	CardOrderingDirection   SnapOperationRank
	CardSuitOrdering        SnapOperationSuit
	CardColorOrdering       SnapOperationColor
}

var Difficulty DifficultySettings
