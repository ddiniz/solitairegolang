package game

import (
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"os"
	c "solitairegolang/source/card"
	h "solitairegolang/source/hitbox"
	u "solitairegolang/source/utils"
	"sort"
)

type SnapOperationSuit uint8
type SnapOperationRank uint8
type SnapOperationColor uint8

const (
	SMALLER_RANK SnapOperationRank = iota
	BIGGER_RANK
	IGNORE_RANK
)

const (
	SAME_SUIT SnapOperationSuit = iota
	DIFFERENT_SUIT
	IGNORE_SUIT
)

const (
	SAME_COLOR SnapOperationColor = iota
	DIFFERENT_COLOR
	IGNORE_COLOR
)

type GameStateEnum uint8

const (
	INITIALIZATION GameStateEnum = iota
	INPUT
	RECALCULATE_BOARD
	END
	RESTART
)

type Game struct {
	Cols         int         `json:"cols"`
	MaxColHeight int         `json:"maxColHeight"`
	Board        [][]*c.Card `json:"board"`

	Foundations [][]*c.Card `json:"foundations"`

	HiddenStock        []*c.Card `json:"hiddenStock"`
	HiddenStockPullQnt int       `json:"hiddenStockPullQnt"`
	HiddenStockPos     *h.Hitbox `json:"hiddenStockPos"`
	Stock              []*c.Card `json:"stock"`

	Hand []*c.Card `json:"hand"`

	EmptyBoardPos    []*h.Hitbox   `json:"emptyBoardPos"`
	CurrentGameState GameStateEnum `json:"currentGameState"`

	OpenStockAvailable bool    `json:"openStockAvailable"`
	OpenStock          *c.Card `json:"openStock"`
}

func NewGame(cols int, maxColHeigth int, hiddenStockPullQnt int) *Game {
	return &Game{
		Cols:               cols,
		MaxColHeight:       maxColHeigth,
		Board:              newBoard(cols),
		Foundations:        make([][]*c.Card, 4),
		HiddenStock:        make([]*c.Card, 0),
		Stock:              make([]*c.Card, 0),
		EmptyBoardPos:      make([]*h.Hitbox, cols),
		OpenStockAvailable: false,
		OpenStock:          nil,
		Hand:               make([]*c.Card, 0),
		HiddenStockPos:     &h.Hitbox{X: 5, Y: 5, W: c.CARD_WIDTH, H: c.CARD_HEIGHT},
		HiddenStockPullQnt: hiddenStockPullQnt,
	}
}

func (game *Game) InitializeGameKlondike() {
	for {
		game.HiddenStock = prepareHiddenStock()
		numCardsPerCol := 1
		noAcesOnTop := true
		for i := 0; i < game.Cols; i++ {
			card := &c.Card{}
			for j := 0; j < numCardsPerCol; j++ {
				card = getCardFromHiddenStock(game)
				if card != nil {
					card.State = c.BOARD
					game.Board[i] = append(game.Board[i], card)
					card.Col = i
					card.Height = j
					continue
				}

				log.Fatalf("Nil Card from HiddenStock when initializing board")
			}
			if card.Rank == c.CardA {
				noAcesOnTop = false
				break
			}
			numCardsPerCol += 1
		}
		if !noAcesOnTop {
			game.Board = make([][]*c.Card, game.Cols)
			for i := 0; i < game.Cols; i++ {
				game.Board[i] = make([]*c.Card, 0)
			}
			continue
		}

		break
	}
}

func printBadState(game *Game, next GameStateEnum) {
	log.Println("Bad game state, current:", game.CurrentGameState, " next:", next)
}

func (game *Game) ChangeState(next GameStateEnum) {
	switch game.CurrentGameState {
	case INITIALIZATION:
		switch next {
		case INPUT:
			game.CurrentGameState = INPUT
		default:
			printBadState(game, next)
		}
	case INPUT:
		switch next {
		case RECALCULATE_BOARD:
			game.CurrentGameState = RECALCULATE_BOARD
		case RESTART:
			game.CurrentGameState = RESTART
		default:
			printBadState(game, next)
		}
	case RECALCULATE_BOARD:
		switch next {
		case INPUT:
			game.CurrentGameState = INPUT
		case END:
			game.CurrentGameState = END
		default:
			printBadState(game, next)
		}
	case END:
		switch next {
		case END:
			game.CurrentGameState = END
		default:
			printBadState(game, next)
		}
	case RESTART:
		switch next {
		case INITIALIZATION:
			game.CurrentGameState = INITIALIZATION
		default:
			printBadState(game, next)
		}
	}
}

func (game *Game) PullCardsHiddenStock() {
	if u.IsEmpty(game.HiddenStock) {
		game.OpenStockAvailable = true
		log.Printf("Illegal move, no more cards in hidden stock.\n")
		return
	}

	Move(&game.HiddenStock, &game.Stock, game.HiddenStockPullQnt)

	if u.IsEmpty(game.HiddenStock) {
		game.OpenStockAvailable = true
	}
}

func Move(from *[]*c.Card, to *[]*c.Card, quantity int) {
	if len(*from) == 0 {
		log.Printf("Trying to move from empty array.")
		return
	}
	if to == nil {
		log.Printf("Trying to move to nil array.")
		return
	}
	*to = append(*to, (*from)[len(*from)-quantity:]...)
	*from = (*from)[:len(*from)-quantity]
}

func (game *Game) MoveCardInBoard(fromCol int, fromColHeight int, toCol int) {
	if fromCol > game.Cols || fromCol < 0 {
		log.Printf("Illegal move, source column doesn't exist: %d\n", fromCol)
		return
	}
	if toCol > game.Cols || toCol < 0 {
		log.Printf("Illegal move, destination column doesn't exist: %d\n", fromColHeight)
		return
	}
	if fromColHeight > game.MaxColHeight {
		log.Printf("Illegal move, source column height is invalid: %d\n", fromColHeight)
		return
	}
	fromColLength := len(game.Board[fromCol])
	if fromColHeight < 0 {
		fromColHeight = fromColLength - 1
	}

	if fromColLength <= fromColHeight {
		log.Printf("Illegal move, source column height is invalid: %d\n", fromColHeight)
		return
	}

	cards := game.Board[fromCol][fromColHeight:]
	if !areCardsSorted(cards) {
		log.Printf("Illegal move, trying to pick cards that aren't sorted:")
		for _, card := range cards {
			log.Printf(card.ToText() + " ; ")
		}
		log.Printf("\n")
	}

	if u.IsEmpty(game.Board[toCol]) {
		game.Board[toCol] = append(game.Board[toCol], cards...)
		game.Board[fromCol] = game.Board[fromCol][:fromColLength-len(cards)]
		return
	}

	lastCard := u.GetLast(game.Board[toCol])
	if lastCard == nil {
		return
	}

	if CanCardSnapToCard(cards[0], lastCard, DIFFERENT_COLOR, SMALLER_RANK, IGNORE_SUIT) {
		game.Board[toCol] = append(game.Board[toCol], cards...)
		game.Board[fromCol] = game.Board[fromCol][:fromColLength-len(cards)]
		return
	}

	source := cards[0].ToText()
	destination := lastCard.ToText()
	log.Printf("Illegal move, source card can't be snapped to destination card. Source card %s ; Destination Card: %s\n", source, destination)
}

func (game *Game) MoveCardFromStock(col int) {
	if col > game.Cols || col < 0 {
		log.Printf("Illegal move, destination column doesn't exist: %d\n", col)
		return
	}
	if u.IsEmpty(game.Stock) {
		log.Printf("Illegal move, no cards on stock\n")
		return
	}
	topStockIndex := len(game.Stock) - 1
	stockTopCard := u.GetLast(game.Stock)
	if stockTopCard == nil {
		return
	}
	if u.IsEmpty(game.Board[col]) {
		game.Board[col] = append(game.Board[col], stockTopCard)
		game.Stock = game.Stock[:topStockIndex]
		return
	}

	boardColTopCard := u.GetLast(game.Board[col])
	if boardColTopCard == nil {
		return
	}
	if CanCardSnapToCard(stockTopCard, boardColTopCard, DIFFERENT_COLOR, SMALLER_RANK, IGNORE_SUIT) {
		game.Board[col] = append(game.Board[col], stockTopCard)
		game.Stock = game.Stock[:topStockIndex]
	}
}

func tryToMoveBoardCardsToFoundations(game *Game, movedCard bool) bool {
	for i, board := range game.Board {
		if u.IsEmpty(board) {
			continue
		}
		for j, foundation := range game.Foundations {
			topIndex := len(board) - 1
			if topIndex < 0 {
				continue
			}

			colTopCard := u.GetLast(board)
			if colTopCard == nil {
				return true
			}
			if len(foundation) > 0 {
				foundationTopCard := u.GetLast(game.Foundations[j])
				if CanCardSnapToCard(colTopCard, foundationTopCard, SAME_COLOR, BIGGER_RANK, SAME_SUIT) {
					game.Foundations[j] = append(game.Foundations[j], colTopCard)
					game.Board[i] = game.Board[i][:topIndex]
					movedCard = true
				}
			} else {
				if colTopCard.Rank == c.CardA {
					game.Foundations[colTopCard.Suit] = append(game.Foundations[colTopCard.Suit], colTopCard)
					game.Board[i] = game.Board[i][:topIndex]
					movedCard = true
				}
				continue
			}

			if colTopCard.Rank == c.CardA {
				game.Foundations[colTopCard.Suit] = append(game.Foundations[colTopCard.Suit], colTopCard)
				game.Board[i] = game.Board[i][:topIndex]
				movedCard = true
			}
		}
	}
	return movedCard
}

func tryToMoveStockCardsToFoundations(game *Game, movedCard bool) bool {
	for _, foundation := range game.Foundations {
		topIndex := len(game.Stock) - 1
		stockTopCard := u.GetLast(game.Stock)
		if stockTopCard == nil {
			return true
		}

		if len(foundation) > 0 {
			foundationTopCard := u.GetLast(foundation)
			if foundationTopCard == nil {
				return true
			}
			if CanCardSnapToCard(stockTopCard, foundationTopCard, SAME_COLOR, BIGGER_RANK, SAME_SUIT) {
				foundation = append(foundation, stockTopCard)
				game.Stock = game.Stock[:topIndex]
				movedCard = true
			}
		} else {
			if stockTopCard.Rank == c.CardA {
				game.Foundations[stockTopCard.Suit] = append(game.Foundations[stockTopCard.Suit], stockTopCard)
				game.Stock = game.Stock[:topIndex]
				movedCard = true
			}
		}
	}
	return movedCard
}

func (game *Game) TryToMoveAllTopCardsToFoundations() {
	for {
		movedCard := tryToMoveBoardCardsToFoundations(game, false)
		if len(game.Stock) != 0 {
			movedCard = tryToMoveStockCardsToFoundations(game, movedCard)
		}
		if !movedCard {
			break
		}
	}
	for i, board := range game.Board {
		for j := range board {
			card := game.Board[i][j]
			card.Height = j
			card.Col = i
		}
	}
}

func (game *Game) IsGameEnded() bool {
	for _, foundation := range game.Foundations {
		if len(foundation) != len(c.Ranks) {
			return false
		}
	}
	return true
}

func (game *Game) GetAllMoveableCards() []*c.Card {
	moveableCards := game.GetBoardMoveableCards()
	stockTop := u.GetLast(game.Stock)
	if stockTop != nil {
		moveableCards = append(moveableCards, stockTop)
	}
	if game.OpenStockAvailable && game.OpenStock != nil {
		moveableCards = append(moveableCards, game.OpenStock)
	}
	sort.Slice(moveableCards, func(i, j int) bool {
		return moveableCards[i].Height > moveableCards[j].Height
	})
	return moveableCards
}

func (game *Game) GetBoardMoveableCards() []*c.Card {
	moveableCards := make([]*c.Card, 0)
	for i, board := range game.Board {
		if len(board) >= 1 {
			card := u.GetLast(game.Board[i])
			if card == nil {
				continue
			}
			moveableCards = append(moveableCards, card)
		}

		for j := len(game.Board[i]) - 2; j >= 0; j-- {
			continuous := false
			card := game.Board[i][j]
			frontCard := game.Board[i][j+1]
			if CanCardSnapToCard(frontCard, card, DIFFERENT_COLOR, SMALLER_RANK, DIFFERENT_SUIT) {
				continuous = true
				moveableCards = append(moveableCards, card)
			}
			if !continuous {
				break
			}
		}
	}
	return moveableCards
}

func (game *Game) GetBoardTopCards() []*c.Card {
	frontCards := make([]*c.Card, 0)
	for _, board := range game.Board {
		if len(board) > 0 {
			card := u.GetLast(board)
			if card == nil {
				continue
			}
			frontCards = append(frontCards, card)
		}
	}
	return frontCards
}

func prepareHiddenStock() []*c.Card {
	stock := make([]*c.Card, 0)
	for _, rank := range c.Ranks {
		for _, suit := range c.Suits {
			card := c.NewEmptyCard(rank, suit)
			stock = append(stock, card)
		}
	}
	rand.Shuffle(len(stock), func(i, j int) { stock[i], stock[j] = stock[j], stock[i] })
	return stock
}

func getCardFromHiddenStock(game *Game) *c.Card {
	stockLen := len(game.HiddenStock)
	endIndex := stockLen - 1
	card := game.HiddenStock[endIndex]
	if stockLen > 0 {
		game.HiddenStock = game.HiddenStock[:endIndex]
		card.ChangeState(c.STOCK)
		return card
	}

	return nil
}

func areCardsSorted(cards []*c.Card) bool {
	if len(cards) <= 1 {
		return true
	}
	for i := 0; i < len(cards)-1; i++ {
		if !CanCardSnapToCard(cards[i+1], cards[i], DIFFERENT_COLOR, SMALLER_RANK, IGNORE_SUIT) {
			return false
		}
	}
	return true
}

func CanCardSnapToCard(source *c.Card, dest *c.Card, colorOperation SnapOperationColor, rankOperation SnapOperationRank, suitOperation SnapOperationSuit) bool {
	if suitOperation == SAME_SUIT {
		if source.Suit != dest.Suit {
			return false
		}
	} else if suitOperation == DIFFERENT_SUIT {
		if source.Suit == dest.Suit {
			return false
		}
	}
	if rankOperation == SMALLER_RANK {
		if source.Rank+1 != dest.Rank {
			return false
		}
	} else if rankOperation == BIGGER_RANK {
		if source.Rank-1 != dest.Rank {
			return false
		}
	}
	if colorOperation == SAME_COLOR {
		if source.Color != dest.Color {
			return false
		}
	} else if colorOperation == DIFFERENT_COLOR {
		if source.Color == dest.Color {
			return false
		}
	}
	return true
}

func newBoard(cols int) [][]*c.Card {
	board := make([][]*c.Card, cols)
	for i := 0; i < cols; i++ {
		board[i] = make([]*c.Card, 0)
	}
	return board
}

func GameToFile(game *Game) {
	filename := "oi.json"
	folder := u.GetGameRootFolder("/")
	readFile, err := os.Create(folder + filename)
	if err != nil {
		fmt.Println(err)
	}
	json, err := json.Marshal(game)
	if err != nil {
		fmt.Println(err)
	}
	readFile.WriteString(string(json))
	readFile.Close()
}

func FileToGame(filename string) *Game {
	folder := u.GetGameRootFolder("/")
	dat, err := os.ReadFile(folder + filename)
	if err != nil {
		fmt.Println(err)
	}
	game := &Game{}
	json.Unmarshal(dat, game)
	return game
}
