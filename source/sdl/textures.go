//go:build sdl
// +build sdl

package sdl

import (
	"log"
	u "solitairegolang/source/utils"

	"github.com/veandco/go-sdl2/img"
	"github.com/veandco/go-sdl2/sdl"
)

const textureSrc = "/textures/default/"

var files = []string{
	"stock",
	"foundation",
	"H_K",
	"H_Q",
	"H_J",
	"H10",
	"H_9",
	"H_8",
	"H_7",
	"H_6",
	"H_5",
	"H_4",
	"H_3",
	"H_2",
	"H_A",
	"S_K",
	"S_Q",
	"S_J",
	"S10",
	"S_9",
	"S_8",
	"S_7",
	"S_6",
	"S_5",
	"S_4",
	"S_3",
	"S_2",
	"S_A",
	"C_K",
	"C_Q",
	"C_J",
	"C10",
	"C_9",
	"C_8",
	"C_7",
	"C_6",
	"C_5",
	"C_4",
	"C_3",
	"C_2",
	"C_A",
	"D_K",
	"D_Q",
	"D_J",
	"D10",
	"D_9",
	"D_8",
	"D_7",
	"D_6",
	"D_5",
	"D_4",
	"D_3",
	"D_2",
	"D_A",
}

func loadImages() map[string]*sdl.Surface {
	folder := u.GetGameRootFolder(textureSrc)
	//folder := "/home/davi/desenvolvimento/personal/solitairegolang/textures/default/"

	images := make(map[string]*sdl.Surface)
	for _, file := range files {
		image, err := img.Load(folder + file + ".png")
		if err != nil {
			log.Fatalf("Failed to load PNG: %s\n", err)
		}
		images[file] = image
	}
	return images
}

func unloadImages(images map[string]*sdl.Surface) {
	for _, value := range images {
		value.Free()
	}
}

func loadTextures(images map[string]*sdl.Surface, renderer *sdl.Renderer) map[string]*sdl.Texture {
	textures := make(map[string]*sdl.Texture)
	for key, value := range images {
		texture, err := renderer.CreateTextureFromSurface(value)
		if err != nil {
			log.Fatalf("Failed to create texture: %s\n", err)
		}
		textures[key] = texture
	}
	return textures
}

func unloadTextures(textures map[string]*sdl.Texture) {
	for _, value := range textures {
		value.Destroy()
	}
}
