//go:build sdl
// +build sdl

package sdl

import (
	c "solitairegolang/source/card"
	g "solitairegolang/source/game"
	h "solitairegolang/source/hitbox"
	u "solitairegolang/source/utils"

	"github.com/veandco/go-sdl2/sdl"
)

func hitboxToRect(hitbox *h.Hitbox) *sdl.Rect {
	return &sdl.Rect{X: hitbox.X, Y: hitbox.Y, W: hitbox.W, H: hitbox.H}
}

func renderBoard(renderer *sdl.Renderer, game *g.Game, textures map[string]*sdl.Texture, posX int32, posY int32) {
	cardOffsetX := int32(10)
	for i, board := range game.Board {
		cardPosX := int32(i) * (c.CARD_WIDTH + cardOffsetX)
		if u.IsEmpty(board) {
			game.EmptyBoardPos[i] = &h.Hitbox{X: posX + cardPosX, Y: posY, W: c.CARD_WIDTH, H: c.CARD_HEIGHT}
			renderer.Copy(
				textures["foundation"],
				&sdl.Rect{X: 0, Y: 0, W: c.CARD_WIDTH, H: c.CARD_HEIGHT},
				hitboxToRect(game.EmptyBoardPos[i]),
			)
		}
	holdingCard:
		for j := range board {
			card := game.Board[i][j]
			if game.Hand != nil {
				for k := 0; k < len(game.Hand); k++ {
					if card == game.Hand[k] {
						break holdingCard
					}
				}
			}
			card.Hitbox = &h.Hitbox{X: posX + cardPosX, Y: posY, W: c.CARD_WIDTH, H: c.CARD_HEIGHT}
			if j > 0 {
				card.Hitbox.Y = game.Board[i][j-1].Hitbox.Y + 30
			}

			renderer.Copy(
				textures[card.TextureName],
				&sdl.Rect{X: 0, Y: 0, W: c.CARD_WIDTH, H: c.CARD_HEIGHT},
				hitboxToRect(card.Hitbox),
			)
		}
	}
}

func renderHiddenStock(renderer *sdl.Renderer, game *g.Game, textures map[string]*sdl.Texture) {
	if len(game.HiddenStock) != 0 {
		renderer.Copy(
			textures["stock"],
			&sdl.Rect{X: 0, Y: 0, W: c.CARD_WIDTH, H: c.CARD_HEIGHT},
			hitboxToRect(game.HiddenStockPos),
		)
		return
	}
	if game.OpenStockAvailable && game.OpenStock != nil && game.OpenStock.State != c.HAND {
		*game.OpenStock.Hitbox = *game.HiddenStockPos
		renderer.Copy(
			textures[game.OpenStock.TextureName],
			&sdl.Rect{X: 0, Y: 0, W: c.CARD_WIDTH, H: c.CARD_HEIGHT},
			hitboxToRect(game.OpenStock.Hitbox),
		)
		return
	}
	renderer.Copy(
		textures["foundation"],
		&sdl.Rect{X: 0, Y: 0, W: c.CARD_WIDTH, H: c.CARD_HEIGHT},
		hitboxToRect(game.HiddenStockPos),
	)
}

func renderStock(renderer *sdl.Renderer, game *g.Game, textures map[string]*sdl.Texture, posX int32, posY int32) {
	for i := range game.Stock {
		card := game.Stock[i]
		card.Hitbox = &h.Hitbox{X: posX, Y: posY, W: c.CARD_WIDTH, H: c.CARD_HEIGHT}
		if i > 0 {
			card.Hitbox.X = game.Stock[i-1].Hitbox.X + 30
		}

		renderer.Copy(
			textures[card.TextureName],
			&sdl.Rect{X: 0, Y: 0, W: c.CARD_WIDTH, H: c.CARD_HEIGHT},
			hitboxToRect(card.Hitbox),
		)
	}
}

func renderFoundations(renderer *sdl.Renderer, game *g.Game, textures map[string]*sdl.Texture, posX int32, posY int32) {
	cardOffsetY := int32(10)
	for i, foundation := range game.Foundations {
		if u.IsEmpty(foundation) {
			cardPosY := int32(i) * (c.CARD_HEIGHT + cardOffsetY)
			renderer.Copy(
				textures["foundation"],
				&sdl.Rect{X: 0, Y: 0, W: c.CARD_WIDTH, H: c.CARD_HEIGHT},
				&sdl.Rect{X: posX, Y: posY + cardPosY, W: c.CARD_WIDTH, H: c.CARD_HEIGHT},
			)

			continue
		}

		card := u.GetLast(game.Foundations[i])
		if card == nil {
			continue
		}
		cardPosY := int32(i) * (c.CARD_HEIGHT + cardOffsetY)
		card.Hitbox = &h.Hitbox{X: posX, Y: posY + cardPosY, W: c.CARD_WIDTH, H: c.CARD_HEIGHT}
		renderer.Copy(
			textures[card.TextureName],
			&sdl.Rect{X: 0, Y: 0, W: c.CARD_WIDTH, H: c.CARD_HEIGHT},
			hitboxToRect(card.Hitbox),
		)
	}
}

func renderHeldCard(renderer *sdl.Renderer, heldCards []*c.Card, textures map[string]*sdl.Texture) {
	for i := range heldCards {
		rect := heldCards[i].Hitbox
		if i > 0 {
			rect.Y = heldCards[i-1].Hitbox.Y + 30
		}
		renderer.Copy(
			textures[heldCards[i].TextureName],
			&sdl.Rect{X: 0, Y: 0, W: c.CARD_WIDTH, H: c.CARD_HEIGHT},
			hitboxToRect(heldCards[i].Hitbox),
		)
	}
}
