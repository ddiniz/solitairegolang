//go:build sdl
// +build sdl

package sdl

import "github.com/veandco/go-sdl2/sdl"

type GameInput struct {
	Quit      bool
	MouseX    int32
	MouseY    int32
	MouseDown bool
	Reset     bool
	Save      bool
	Load      bool
}

var Input = &GameInput{false, 0, 0, false, false, false, false}

func GetInput() {
	Input.MouseDown = false
	Input.Reset = false
	Input.Quit = false
	Input.Save = false
	Input.Load = false
	for event := sdl.PollEvent(); event != nil; event = sdl.PollEvent() {
		switch t := event.(type) {
		case *sdl.QuitEvent:
			Input.Quit = true
		case *sdl.MouseMotionEvent:
			Input.MouseX = t.X
			Input.MouseY = t.Y
		case *sdl.MouseButtonEvent:
			if t.Type == sdl.MOUSEBUTTONDOWN && t.State == sdl.PRESSED {
				Input.MouseDown = true
			}
		case *sdl.KeyboardEvent:
			if t.Type == sdl.KEYDOWN && t.State == sdl.PRESSED {
				if t.Keysym.Scancode == sdl.SCANCODE_R {
					Input.Reset = true
					break
				}
				if t.Keysym.Sym == sdl.K_ESCAPE {
					Input.Quit = true
				} else if t.Keysym.Scancode == sdl.SCANCODE_S {
					Input.Save = true
				} else if t.Keysym.Scancode == sdl.SCANCODE_L {
					Input.Load = true
				}
			}
		}
	}
}
