//go:build sdl
// +build sdl

package sdl

import (
	"fmt"
	"os"
	c "solitairegolang/source/card"
	g "solitairegolang/source/game"

	"github.com/veandco/go-sdl2/sdl"
)

const (
	WINDOW_HEIGHT = int32(1060)
	WINDOW_WIDTH  = int32(768)
)

func RunGame() {
	window, renderer, _ := initializeSDL("Solitaire Golang", WINDOW_HEIGHT, WINDOW_WIDTH)
	defer sdl.Quit()
	defer window.Destroy()
	defer renderer.Destroy()
	images := loadImages()
	textures := loadTextures(images, renderer)
	defer unloadImages(images)
	defer unloadTextures(textures)

	sdl.JoystickEventState(sdl.ENABLE)
	game := &g.Game{}

	for !Input.Quit {
		renderer.Clear()
		renderer.SetDrawColor(255, 255, 255, 255)
		renderer.FillRect(&sdl.Rect{X: 0, Y: 0, W: WINDOW_HEIGHT, H: WINDOW_WIDTH})

		executeGameLoop(game)

		renderHiddenStock(renderer, game, textures)
		renderStock(renderer, game, textures, 127, 5)
		renderBoard(renderer, game, textures, 5, 200)
		renderFoundations(renderer, game, textures, (int32(game.Cols)*c.CARD_WIDTH)+160, 5)
		if game.Hand != nil {
			renderHeldCard(renderer, game.Hand, textures)
		}

		renderer.Present()
		sdl.Delay(16)
	}
}

func initializeSDL(winTitle string, winWidth int32, winHeight int32) (*sdl.Window, *sdl.Renderer, error) {
	if err := sdl.Init(sdl.INIT_EVERYTHING); err != nil {
		panic(err)
	}
	window, err := sdl.CreateWindow(winTitle, sdl.WINDOWPOS_UNDEFINED, sdl.WINDOWPOS_UNDEFINED,
		winWidth, winHeight, sdl.WINDOW_SHOWN)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to create window: %s\n", err)
		return nil, nil, err
	}

	renderer, err := sdl.CreateRenderer(window, -1, sdl.RENDERER_ACCELERATED)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to create renderer: %s\n", err)
		return nil, nil, err
	}
	return window, renderer, err
}
