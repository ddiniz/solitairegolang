//go:build sdl
// +build sdl

package sdl

import (
	"log"
	c "solitairegolang/source/card"
	g "solitairegolang/source/game"
	u "solitairegolang/source/utils"
)

func executeGameLoop(game *g.Game) {
	switch game.CurrentGameState {
	case g.INITIALIZATION:
		log.Println("INITIALIZATION")
		*game = *g.NewGame(7, 21, 3)
		game.InitializeGameKlondike()
		game.ChangeState(g.INPUT)
	case g.INPUT:
		GetInput()
		if Input.Save {
			log.Println("SAVED GAME")
			g.GameToFile(game)
		}
		if Input.Reset {
			game.ChangeState(g.RESTART)
		}
		if Input.Load {
			log.Println("LOADED GAME")
			*game = *g.FileToGame("oi.json")
			game.ChangeState(g.INPUT)
		}
		updateHeldCardsRederingPosition(game)
		if Input.MouseDown {
			recalculate := handleCardClicking(game)
			if recalculate {
				game.ChangeState(g.RECALCULATE_BOARD)
			}
		}
	case g.RECALCULATE_BOARD:
		log.Println("RECALCULATE_BOARD")
		game.TryToMoveAllTopCardsToFoundations()
		ended := game.IsGameEnded()
		if ended {
			game.ChangeState(g.END)
			break
		}

		game.ChangeState(g.INPUT)

	case g.END:
		log.Println("END")
		game.ChangeState(g.RESTART)
	case g.RESTART:
		log.Println("RESTART")
		game.ChangeState(g.INITIALIZATION)
	}
}

func handleCardClicking(game *g.Game) bool {
	if u.IsEmpty(game.Hand) {
		return handleClickingWhileNotHoldingCards(game)
	}
	return handleClickingWhileHoldingCards(game)
}

func handleClickingWhileNotHoldingCards(game *g.Game) bool {
	if !game.OpenStockAvailable && game.HiddenStockPos.InHitbox(Input.MouseX, Input.MouseY) {
		game.PullCardsHiddenStock()
		//TODO: only change state of cards that were pulled, not the whole stock
		for _, card := range game.Stock {
			card.ChangeState(c.STOCK)
		}
		return true
	} else {
		moveableCards := game.GetAllMoveableCards()
		for _, card := range moveableCards {
			if !card.Hitbox.InHitbox(Input.MouseX, Input.MouseY) {
				continue
			}
			if card.State == c.STOCK {
				g.Move(&game.Stock, &game.Hand, 1)
				card.ChangeState(c.HAND)
				return false
			}
			if card.State == c.OPEN_STOCK {
				game.OpenStock.ChangeState(c.HAND)
				game.Hand = append(game.Hand, game.OpenStock)
				game.OpenStock = nil
				return false
			}
			cardsToBeHeld := game.Board[card.Col][card.Height:]
			for j := 0; j < len(cardsToBeHeld); j++ {
				cardsToBeHeld[j].ChangeState(c.HAND)
			}
			g.Move(&game.Board[card.Col], &game.Hand, len(cardsToBeHeld))
			return false
		}
	}
	return false
}

func handleClickingWhileHoldingCards(game *g.Game) bool {
	frontCards := game.GetBoardTopCards()
	targetCard := getClickedCard(frontCards, Input.MouseX, Input.MouseY)
	if targetCard != nil && targetCard.State == c.BOARD {
		if g.CanCardSnapToCard(game.Hand[0], targetCard, g.DIFFERENT_COLOR, g.SMALLER_RANK, g.DIFFERENT_SUIT) {
			for _, card := range game.Hand {
				card.ChangeState(c.BOARD)
			}
			g.Move(&game.Hand, &game.Board[targetCard.Col], len(game.Hand))
			return true
		}
	} else {
		for i := range game.EmptyBoardPos {
			if game.EmptyBoardPos[i].InHitbox(Input.MouseX, Input.MouseY) {
				for _, card := range game.Hand {
					card.ChangeState(c.BOARD)
				}
				g.Move(&game.Hand, &game.Board[i], len(game.Hand))
				return true
			}
		}
		if game.OpenStockAvailable && game.OpenStock == nil && len(game.Hand) == 1 && game.HiddenStockPos.InHitbox(Input.MouseX, Input.MouseY) {
			game.OpenStock = game.Hand[0]
			game.Hand = game.Hand[:0]
			game.OpenStock.ChangeState(c.OPEN_STOCK)
			return true
		}
	}
	//clicked nowhere
	ReturnHandCardsToLastPosition(game)
	return false
}

func ReturnHandCardsToLastPosition(game *g.Game) {
	firstCard := game.Hand[0]
	if firstCard.LastState == c.BOARD {
		for _, card := range game.Hand {
			card.ChangeState(c.BOARD)
		}
		g.Move(&game.Hand, &game.Board[firstCard.Col], len(game.Hand))
	} else {
		if firstCard.LastState == c.STOCK {
			for _, card := range game.Hand {
				card.ChangeState(c.STOCK)
			}
			g.Move(&game.Hand, &game.Stock, len(game.Hand))
		} else if firstCard.LastState == c.OPEN_STOCK {
			game.OpenStock = game.Hand[0]
			game.Hand = game.Hand[:0]
			game.OpenStock.ChangeState(c.OPEN_STOCK)
		}
	}
}

func updateHeldCardsRederingPosition(game *g.Game) {
	for i := range game.Hand {
		heldCard := game.Hand[i]
		heldCard.Hitbox.X = Input.MouseX
		heldCard.Hitbox.Y = Input.MouseY
	}
}

func getClickedCard(cards []*c.Card, mouseX int32, mouseY int32) *c.Card {
	for _, card := range cards {
		if card.State != c.HAND && card.Hitbox.InHitbox(mouseX, mouseY) {
			return card
		}
	}
	return nil
}
